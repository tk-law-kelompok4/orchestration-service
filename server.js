import { createServer } from "http"
import { Server as SocketServer } from "socket.io"
import { createClient } from 'redis';

import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import grpc from '@grpc/grpc-js'
import protoLoader from '@grpc/proto-loader'
import amqp from 'amqplib/callback_api.js'
import path from 'path'
import {fileURLToPath} from 'url';
import { createLogger, transports, format } from 'winston';
import 'winston-mongodb'

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const EXPRESS_PORT = 3000
const HELLOWORLD_GRPC_PORT = 50051
const OAUTH_GRPC_PORT = 50052
const ORDER_GRPC_PORT = 50053
const TRANSACTION_GRPC_PORT = 50054

const SOCKET_PORT = 3001

const redisClient = createClient();

redisClient.on('error', (err) => console.log('Redis Client Error', err));

await redisClient.connect();

let RABBITMQ = null
const STATUS_UPDATE_QUEUE = 'StatusUpdate'

// log
// log4js.configure({
//     appenders: { cheese: { type: "log4js-node-mongodb", 
//                             connectionString: 'mongodb+srv://PIZZAKU:Hthg027n2uTubGZa@cluster0.rrcy1.mongodb.net/?retryWrites=true&w=majority',
//                             collectionName: 'logs'
//                         } },
//     categories: { default: { appenders: ["cheese"], level: "error" } }
//   });
// const mongoClient = new MongoClient('mongodb+srv://PIZZAKU:Hthg027n2uTubGZa@cluster0.rrcy1.mongodb.net/?retryWrites=true&w=majority');
// async function runMongo() {
//     try {
//       await mongoClient.connect();
//       console.log("CONNECTED TO MONGO")
//     } finally {
//       // Ensures that the client will close when you finish/error
//       await mongoClient.close();
//     }
//   }

const logger = createLogger({
    transports: [
        new transports.Console({
            level: 'info',
            format: format.combine(format.timestamp(), format.json())
        }),
        new transports.MongoDB({
            level: 'error',
            db: "mongodb+srv://PIZZAKU:Hthg027n2uTubGZa@cluster0.rrcy1.mongodb.net/logging-service?retryWrites=true&w=majority",
            // db: "mongodb+srv://kefassatrio:9ze559AMMBsToTTd@cluster0.hwob7.mongodb.net/lab4?retryWrites=true&w=majority",
            collection: 'logs',
            defaultMeta: { service: 'orchestration-service' },
            format: format.combine(format.timestamp(), format.json())
        }),
        new transports.MongoDB({
            level: 'info',
            db: "mongodb+srv://PIZZAKU:Hthg027n2uTubGZa@cluster0.rrcy1.mongodb.net/logging-service?retryWrites=true&w=majority",
            // db: "mongodb+srv://kefassatrio:9ze559AMMBsToTTd@cluster0.hwob7.mongodb.net/lab4?retryWrites=true&w=majority",
            collection: 'logs',
            format: format.combine(format.timestamp(), format.json())
        })
    ]
})

// GRPC
const HELLOWORLD_PROTO_PATH = __dirname + '/protos/helloworld.proto'
const OAUTH_PROTO_PATH = __dirname + '/protos/oauth.proto'
const ORDER_PROTO_PATH = __dirname + '/protos/orderservice.proto'
const TRANSACTION_PROTO_PATH = __dirname + '/protos/transaction.proto'

// hello world
const helloWorldPackageDefinition = protoLoader.loadSync(
    HELLOWORLD_PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
)
const hello_proto = grpc.loadPackageDefinition(helloWorldPackageDefinition).helloworld
const helloWorldTarget = 'localhost:' + HELLOWORLD_GRPC_PORT
const helloWorldclient = new hello_proto.Greeter(helloWorldTarget,
    grpc.credentials.createInsecure());

// oauth    
const oAuthPackageDefinition = protoLoader.loadSync(
    OAUTH_PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
)
const oauth_proto = grpc.loadPackageDefinition(oAuthPackageDefinition).oauth
const oAuthTarget = 'localhost:' + OAUTH_GRPC_PORT
const oAuthclient = new oauth_proto.OAuth(oAuthTarget,
    grpc.credentials.createInsecure());

// order
const orderPackageDefinition = protoLoader.loadSync(
    ORDER_PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
)
const order_proto = grpc.loadPackageDefinition(orderPackageDefinition).orderservice
const orderTarget = 'localhost:' + ORDER_GRPC_PORT
const orderclient = new order_proto.OrderService(orderTarget,
    grpc.credentials.createInsecure()
);

// transaction
const transactionPackageDefinition = protoLoader.loadSync(
    TRANSACTION_PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
)
const transaction_proto = grpc.loadPackageDefinition(transactionPackageDefinition).transaction
const transactionTarget = 'localhost:' + TRANSACTION_GRPC_PORT
const transactionclient = new transaction_proto.Transaction(transactionTarget,
    grpc.credentials.createInsecure()
);

// EXPRESS
const expressApp = express()
expressApp.use(cors({credentials: true, origin: true}))
expressApp.use(bodyParser.urlencoded({ extended: false }))
expressApp.use(bodyParser.json())

expressApp.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// hello world
expressApp.get('/', (req, res) => {
    helloWorldclient.sayHello({name: "World"}, function(err, response) {
        logger.info({message: "[orchestration-service] Hello World"})
        res.status(200).send("helloo")
    });
})

// oauth
expressApp.post('/api/oauth/register', (req, res) => {
    oAuthclient.register({
        "username": req.body.username,
        "password": req.body.password,
        "role": req.body.role
      }, function(err, response) {
        if(!response) {
            logger.error(`[orchestration-service] registration failed, status: 500, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({"message": "no response"})}`)
            return res.status(500).json({
                "message": "no response"
            })
        }
        if(response.status != 201) {
            logger.error(`[orchestration-service] registration failed, status: ${response.status}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
            return res.status(response.status).json({
                "message": response.message
            })
        }
        logger.info(`[orchestration-service] registration successful, status: ${response.status}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
        res.status(201).send()
    })
})
expressApp.post('/api/oauth/login', (req, res) => {
    oAuthclient.login({
        "username": req.body.username,
        "password": req.body.password,
      }, function(err, response) {
        if(!response) {
            logger.error(`[orchestration-service] login failed, status: 500, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({"message": "no response"})}`)
            return res.status(500).json({
                "message": "no response"
            })
        }
        if(response.status != 200) {
            logger.error(`[orchestration-service] login unsuccessful, status: ${response.status}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
            return res.status(response.status).json({
                "message": response.message
            })
        }
        logger.info(`[orchestration-service] login successful, status: ${response.status}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
        res.status(200).json({
            "accessToken" : response.accessToken,
            "expiresIn" : response.expiresIn,
            "tokenType" : response.tokenType,
            "scope" : response.scope
        })
    })
})
expressApp.get('/api/oauth/current-user', (req, res) => {
    if (!req.headers.authorization || !req.headers.authorization.includes("Bearer ")) {
        logger.error(`[orchestration-service] getting user info unsuccessful, status: 400, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({ "message": 'token invalid' })}`)
        return res.status(400).json({ "message": 'token invalid' });
    }
    oAuthclient.currentUser({
        "accessToken": req.headers.authorization.split(" ")[1]
    }, function(err, response) {
        if(!response) {
            logger.error(`[orchestration-service] get user info failed, status: 500, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({"message": "no response"})}`)
            return res.status(500).json({
                "message": "no response"
            })
        }
        if(response.status != 200) {
            logger.error(`[orchestration-service] getting user info unsuccessful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
            return res.status(response.status).json({
                "message": response.message
            })
        }
        logger.info(`[orchestration-service] getting user info successful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
        res.status(200).json({
            "username" : response.username,
            "role" : response.role
        })
    })
})
expressApp.post('/api/oauth/logout', (req, res) => {
    if (!req.headers.authorization || !req.headers.authorization.includes("Bearer ")) {
        logger.error(`[orchestration-service] logout unsuccessful, status: 400, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({ "message": 'token invalid' })}`)
        return res.status(400).json({ "message": 'token invalid' });
    }
    oAuthclient.logout({
        "accessToken": req.headers.authorization.split(" ")[1]
    }, function(err, response) {
        if(!response) {
            logger.error(`[orchestration-service] logout failed, status: 500, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({"message": "no response"})}`)
            return res.status(500).json({
                "message": "no response"
            })
        }
        if(response.status != 204) {
            logger.error(`[orchestration-service] logout unsuccessful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
            return res.status(response.status).json({
                "message": response.message
            })
        }
        logger.info(`[orchestration-service] logout successful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
        res.status(204).send()
    })
})

// order
expressApp.get('/api/menu', async (req, res) => {
    if (!req.headers.authorization || !req.headers.authorization.includes("Bearer ")) {
        logger.error(`[orchestration-service] getting menu unsuccessful, status: 400, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({ "message": 'token invalid' })}`)
        return res.status(400).json({ "message": 'token invalid' });
    }
    const menuCache = await redisClient.get('menu')
    console.log(menuCache)
    if(menuCache) {
        logger.info(`[orchestration-service] getting menu successful, status: 200, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${menuCache}`)
        return res.status(200).json(JSON.parse(menuCache))
    }

    orderclient.menu({
        "accessToken": req.headers.authorization.split(" ")[1]
    }, async function(err, response) {
        if(!response) {
            logger.error(`[orchestration-service] get menu failed, status: 500, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({"message": "no response"})}`)
            return res.status(500).json({
                "message": "no response"
            })
        }
        if(response.status != 200) {
        logger.error(`[orchestration-service] getting menu unsuccessful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
            return res.status(response.status).json({
                "message": response.message
            })
        }
        const menuResponse = {
            "items" : response.items
        }
        await redisClient.set("menu", JSON.stringify(menuResponse))
        logger.info(`[orchestration-service] getting menu successful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
        res.status(200).json(menuResponse)
    });
})

expressApp.post('/api/orders', (req, res) => {
    if (!req.headers.authorization || !req.headers.authorization.includes("Bearer ")) {
        logger.error(`[orchestration-service] creating order unsuccessful, status: 400, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({ "message": 'token invalid' })}`)
        return res.status(400).json({ "message": 'token invalid' });
    }
    orderclient.createOrder({
        "accessToken": req.headers.authorization.split(" ")[1],
        "items": req.body.items
    }, function(err, response) {
        if(!response) {
            logger.error(`[orchestration-service] create order failed, status: 500, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({"message": "no response"})}`)
            return res.status(500).json({
                "message": "no response"
            })
        }
        if(response.status != 201) {
        logger.error(`[orchestration-service] creating order unsuccessful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
            return res.status(response.status).json({
                "message": response.message
            })
        }
        logger.info(`[orchestration-service] creating order successful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
        res.status(201).json({
            "id" : response.id,
            "status": response.orderStatus,
            "items": response.items
        })
    });
})

expressApp.get('/api/orders', (req, res) => {
    if (!req.headers.authorization || !req.headers.authorization.includes("Bearer ")) {
        logger.error(`[orchestration-service] getting orders unsuccessful, status: 400, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({ "message": 'token invalid' })}`)
        return res.status(400).json({ "message": 'token invalid' });
    }
    orderclient.getOrders({
        "accessToken": req.headers.authorization.split(" ")[1]
    }, function(err, response) {
        if(!response) {
            logger.error(`[orchestration-service] get orders failed, status: 500, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({"message": "no response"})}`)
            return res.status(500).json({
                "message": "no response"
            })
        }
        if(response.status != 200) {
        logger.error(`[orchestration-service] getting orders unsuccessful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)})}`)
            return res.status(response.status).json({
                "message": response.message
            })
        }
        logger.info(`[orchestration-service] getting orders successful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
        res.status(200).json({
            "data" : response.data
        })
    });
})

expressApp.get('/api/orders/:orderId', (req, res) => {
    if (!req.headers.authorization || !req.headers.authorization.includes("Bearer ")) {
        logger.error(`[orchestration-service] getting order unsuccessful, status: 400, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({ "message": 'token invalid' })}`)
        return res.status(400).json({ "message": 'token invalid' });
    }
    orderclient.getOrder({
        "accessToken": req.headers.authorization.split(" ")[1],
        "orderId": req.params.orderId
    }, function(err, response) {
        if(!response) {
            logger.error(`[orchestration-service] get order failed, status: 500, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({"message": "no response"})}`)
            return res.status(500).json({
                "message": "no response"
            })
        }
        if(response.status != 200) {
            logger.error(`[orchestration-service] getting order unsuccessful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)})}`)
            return res.status(response.status).json({
                "message": response.message
            })
        }
        logger.info(`[orchestration-service] getting order successful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
        res.status(200).json(response.order)
    });
})

expressApp.put('/api/orders/:orderId', (req, res) => {
    if (!req.headers.authorization || !req.headers.authorization.includes("Bearer ")) {
        logger.error(`[orchestration-service] updating order unsuccessful, status: 400, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({ "message": 'token invalid' })}`)
        return res.status(400).json({ "message": 'token invalid' });
    }
    orderclient.updateOrder({
        "accessToken": req.headers.authorization.split(" ")[1],
        "orderId": req.params.orderId,
        "orderStatus": req.body.status
    }, function(err, response) {
        if(!response) {
            logger.error(`[orchestration-service] update order failed, status: 500, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({"message": "no response"})}`)
            return res.status(500).json({
                "message": "no response"
            })
        }
        if(response.status != 200) {
            logger.error(`[orchestration-service] updating order unsuccessful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)})}`)
            return res.status(response.status).json({
                "message": response.message
            })
        }
        logger.info(`[orchestration-service] updating order successful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
        res.status(200).json(response.order)
    });
})

// transaction
expressApp.post('/api/transaction', (req, res) => {
    if (!req.headers.authorization || !req.headers.authorization.includes("Bearer ")) {
        logger.error(`[orchestration-service] transaction unsuccessful, status: 400, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({ "message": 'token invalid' })}`)
        return res.status(400).json({ "message": 'token invalid' });
    }
    transactionclient.pay({
        "accessToken": req.headers.authorization.split(" ")[1],
        "orderId": req.body.orderId
    }, function(err, response) {
        if(!response) {
            logger.error(`[orchestration-service] transaction failed, status: 500, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify({"message": "no response"})}`)
            return res.status(500).json({
                "message": "no response"
            })
        }
        if(response.status != 200) {
            logger.error(`[orchestration-service] transaction unsuccessful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)})}`)
            return res.status(response.status).json({
                "message": response.message
            })
        }
        logger.info(`[orchestration-service] transaction successful, status: ${response.status}, authorization: ${req.headers.authorization}, request: ${JSON.stringify(req.body)}, response: ${JSON.stringify(response)}`)
        res.status(200).json(response.order)
    });
})

// SOCKET.IO
const socketServer = createServer()

const io = new SocketServer(socketServer, {
    path: "/socket.io/",
    cors: {
        origin: '*',
    }
})

io.on("connection", (socket) => {
    console.log("New client connected");
    socket.on("joinRoom", username => socket.join(username))

    socket.on("disconnect", () => {
        console.log("Client disconnected");
    })
})

const startExpress = () => {
    expressApp.listen(EXPRESS_PORT, () => console.log(`[orchestration service] (express app) listening on port ${EXPRESS_PORT}!`))
}

const startSocket = () => {
    socketServer.listen(SOCKET_PORT)
    console.log(`orchestration service (socket.io server) listening on port ${SOCKET_PORT}!`)
}

const startRabbitMQ = () => {
    amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0
    }
    RABBITMQ = connection.createChannel(function(error1, channel) {
        if (error1) {
        throw error1
        }
        channel.assertQueue(STATUS_UPDATE_QUEUE, {
        durable: false
        })
        console.log(`RabbitMQ connected!`)
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", STATUS_UPDATE_QUEUE)        
    })

    RABBITMQ.consume(STATUS_UPDATE_QUEUE, function(msg) {
        console.log(" [x] Received %s", msg.content.toString())
        const msgJson = JSON.parse(msg.content.toString())
        io.to(msgJson.username).emit(STATUS_UPDATE_QUEUE, msgJson);
    }, {
        noAck: true
    })

    })

}

startExpress()
startSocket()
startRabbitMQ()
